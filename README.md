# Getting started
## 1. [Install node.js](http://nodejs.org/)
## 2. Install [gulp.js](http://gulpjs.com/)
`npm install --global gulp`
## 3. Install gulp modules
Run `npm i` in project folder
## 4. Run gulp
`gulp`

# What inside?
* [Jade](http://jade-lang.com/)
* [CoffeeScript](http://coffeescript.org/)
* [Scss](http://sass-lang.com/)
* Web-server/livereload `http://localhost:8001/`