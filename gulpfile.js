"use strict";

// Gulp plugins
var gulp = require('gulp'),
    devDir = 'dev',
    buildDir = 'assets',
    concat = require('gulp-concat'),
    plumber = require('gulp-plumber'),
    sass = require('gulp-sass'),
    jade = require('gulp-jade'),
    coffee = require('gulp-coffee'),
    uglify = require('gulp-uglify'),
    gulpif = require('gulp-if'),
    autoprefixer = require('gulp-autoprefixer'),
    minifyCSS = require('gulp-minify-css'),
    connect = require('gulp-connect');

// Sources
var src = {
  'scss': ['dev/scss/app.scss'],
  'jade': [
    'dev/jade/**/*.jade', 
    '!dev/jade/partials/**/*.jade', 
    '!dev/jade/layouts/**/*.jade'
  ],
  'js': [
    'dev/js/libs/**/*.js',
    'dev/js/coffee/**/*.coffee'
  ]
}

// Dests
var dest = {
  'scss': 'assets/css',
  'jade': './',
  'js'  : 'assets/js'
}

// Watches
var watch = {
  'scss': 'dev/scss/**/*.scss',
  'jade': 'dev/jade/**/*.jade',
  'js'  : [
    'dev/js/**/*.js',
    'dev/js/**/*.coffee'
  ]
} 

// Server connect
gulp.task('connect', function() {
  connect.server({
    port: 8001,
    livereload: true
  });
});

// CSS
gulp.task('scss', function () {
  gulp.src(src.scss)
    .pipe(plumber())
    .pipe(sass({errLogToConsole: true}))
    .pipe(autoprefixer('> 1%', 'last 2 version', 'ie 9', 'ios 6', 'android 4'))
    // .pipe(minifyCSS())
    .pipe(gulp.dest(dest.scss))
    .pipe(connect.reload());
});

// HTML
gulp.task('jade', function() {
  gulp.src(src.jade)
    .pipe(plumber())
    .pipe(jade({pretty: true}))
    .pipe(gulp.dest(dest.jade))
    .pipe(connect.reload())
});

// JS
gulp.task('js', function() {
  gulp.src(src.js)
    .pipe(plumber())
    .pipe(gulpif(/[.]coffee$/, coffee()))
    .pipe(concat('app.js'))
    // .pipe(uglify())
    .pipe(gulp.dest(dest.js))
    .pipe(connect.reload())
});


// Watch
gulp.task('watch', function() {
  gulp.watch(watch.jade, ['jade']);
  gulp.watch(watch.scss, ['scss']);
  gulp.watch(watch.js,   ['js']);
});

// Default
gulp.task('default', ['scss', 'jade', 'js', 'watch', 'connect']);